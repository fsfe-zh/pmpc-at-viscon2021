% Intended LaTeX compiler: pdflatex
\documentclass[t, 11pt]{beamer}  % add handout to the options to get slides w/o pauses

\usepackage{pgfpages}
%\setbeameroption{show notes} % show slides with notes below
%\setbeameroption{show notes on second screen=right} % show slides with notes to the right
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref} % include hyperlinks
\usepackage{booktabs}
\usepackage{caption}
\usepackage{ccicons}
\captionsetup[figure]{labelformat=empty}

\usetheme{}

\logo{\includegraphics[height=1.0cm]{./res/logo.png}}

\author{ALEXANDER PITSCH\\{alexander.pitsch@fsfe.org}}

\institute{Free Software Foundation Europe e.V. - local group Zurich}

\title{PUBLIC MONEY? \\ PUBLIC CODE!}

\subtitle{Modernising Public Infrastructure with Free Software}

\date{16.10.2021}

\definecolor{fsfe_blue}{RGB}{32,45,121}
\definecolor{fsfe_bluedark}{RGB}{51, 148, 206}
\definecolor{fsfe_bluemedium}{RGB}{74, 21, 240}

\setbeamertemplate{title page}[default]
\setbeamertemplate{titlelike}[shadow=false, bg=white]
\setbeamertemplate{titlesubtitle}[shadow=false, bg=white]
\setbeamertemplate{author}[shadow=false, bg=white]
\setbeamertemplate{institute}[shadow=false, bg=white]
\setbeamertemplate{footline}{\includegraphics[width=\textwidth, height=0.2cm]{res/footer.png}}
\setbeamertemplate{date}[shadow=false, bg=white]

\setbeamercolor*{palette primary}{fg=white, bg=fsfe_blue!75!white}
\setbeamercolor*{palette secondary}{fg=white, bg=fsfe_blue}
\setbeamercolor*{palette tertiary}{fg=white, bg=fsfe_bluemedium}
\setbeamercolor*{palette quaternary}{fg=yellow,bg=gray!5!white}
\setbeamercolor*{palette sidebar primary}{fg=white, bg=fsfe_blue!75!white}
\setbeamercolor*{palette sidebar secondary}{fg=white, bg=fsfe_fsfe_blue}
\setbeamercolor*{palette sidebar tertiary}{fg=white, bg=fsfe_bluemedium}
\setbeamercolor*{palette sidebar quaternary}{fg=yellow,bg=gray!5!white}
\setbeamercolor{title page}{fg=white, bg=fsfe_blue}
\setbeamercolor{titlelike}{fg=fsfe_blue, bg=white}
\setbeamercolor{titlesubtitle}{fg=white, bg=white}
\setbeamercolor{author}{fg=fsfe_bluedark, bg=white}
\setbeamercolor{institute}{fg=white, bg=white}
\setbeamercolor{date}{fg=white, bg=white}
\setbeamercolor{itemize item}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{itemize subitem}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{itemize subsubitem}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{enumerate item}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{enumerate subitem}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{enmerate subitem}{fg=fsfe_blue, bg = fsfe_blue}
\setbeamercolor{structure}{fg=fsfe_blue}
\setbeamercolor{frametitle}{fg=fsfe_blue, bg=white}
\setbeamercolor{normal text}{fg=fsfe_blue, bg=white}
\setbeamercolor{block title}{fg=white,bg=fsfe_bluedark}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{title page}{size=\large}
\setbeamerfont{title}{size=\Huge}
\setbeamerfont{footnote}{size=\tiny}
\setbeamerfont{author}{size=\footnotesize}

\hypersetup{
  pdfauthor={Alexander Pitsch},
  pdftitle={Public Money? Public Code! @ VIScon 2021},
  pdfkeywords={Free Software; FSFE},
  pdfsubject={},
  pdfcreator={}, 
  pdflang={English}}

\usenavigationsymbolstemplate{} % To remove the navigation symbols

\AtBeginSection[]
{
    \begin{frame}[c]
        \frametitle{Table of Contents}
        \tableofcontents[currentsection]
    \end{frame}
}

\AtBeginSubsection[]
{
    \begin{frame}[c]
        \frametitle{Table of Contents}
        \tableofcontents[currentsection,currentsubsection]
    \end{frame}
}

\begin{document}

\begin{frame}[c]
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{res/infrastructures_large.png}
    \caption{Infrastructures\\\footnotesize{\url{https://xkcd.com/743/}}}
  \end{figure}
\end{frame}

\begin{frame}
  \maketitle
  \tiny{VIScon 2021 $\cdot$ \insertdate} \\
  \tiny{\insertinstitute} \\
  \tiny{\url{https://git.fsfe.org/fsfe-zh/pmpc-at-viscon2021} $\cdot$ \ccbysa}
\end{frame}

\begin{frame}[c]
  \begin{block}{\texttt{\$ whoami}}
    \begin{itemize}
      \pause
      \item{Alex} \pause
      \item{software developer} \pause
      \item{Free Software Foundation Europe [FSFE] supporter} \pause
      \item{member of the FSFE local group Zurich}
    \end{itemize}
  \end{block}
  \note{we meet every $2^{nd}$ Thursday of a month}
\end{frame}

\begin{frame}[c]
  \begin{block}{\texttt{\$ info fsfe}}
    \begin{itemize}
      \pause
      \item{``a charity that empowers users to control technology''} \pause
      \item{non-profit \& non-governmental} \pause
      \item{truly European} \pause
      \item{active since 2001} \pause
      \item{still going strong}
    \end{itemize}
    \note[item]{legally setup as a German association; but active local groups in Spain, France, UK, Germany, Switzerland, Italy, Austria, Poland}
    \note[item]{celebrated our $20^{th}$ anniversary this year}
    \note[item]{many activities whereby the main focus is on public awareness, policy advocacy, and legal support for all matters related to Free Software}
  \end{block}
\end{frame}

\begin{frame}[c]
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}


\section{What is Free Software?}

\begin{frame}[c]
  \frametitle{What is Free Software?}
  \pause
  \begin{block}{The Four Freedoms of Free Software}
    \begin{enumerate}
      \pause
      \item[0 -] use \pause
      \item[1 -] study \pause
      \item[2 -] share \pause
      \item[3 -] improve \pause
    \end{enumerate}
    Provided by a Free Software license.
  \end{block}
  \note[item]{use: freedom to run the program as you wish, for any purpose without limitations like license fees or geographical restrictions.}
  \note[item]{study: freedom to study how the program works, and change it so it does your computing as you wish}
  \note[item]{share: freedom to redistribute copies of the program}
  \note[item]{improve: freedom to distribute copies of your modified versions to others}
  \note[item]{License as a legal document providing you with these rights.}
\end{frame}

\section{Public Money? Public Code!}

\subsection{What?}

\begin{frame}[c]
  \begin{figure}
    \includegraphics[height=0.8\textheight]{res/pmpc_logo_full.jpg}
  \end{figure}
\end{frame}

\begin{frame}[c]
  \frametitle{Goal}
  \begin{quote}
    \raggedright
    We want \textbf{legislation} requiring that \textbf{publicly financed software} developed for the public sector be made \textbf{publicly available} under a Free and Open Source Software license. \\~\\  If it is public money, it should be public code as well.
  \end{quote}
\end{frame}

\subsection{Why?}

\begin{frame}[c]
  \frametitle{For all kinds of reasons!}
  \pause
  \begin{columns}[t]
    
    \begin{column}{0.31\textwidth}
      \begin{block}{economic}
        \begin{itemize}
        \pause
        \item \sout{license fees} \\ tax savings  \pause
        \item \sout{duplicated code} collaboration \pause
        \item \sout{vendor lock-in} independence \pause
        \item \sout{monopoly} competition \pause
        \item \sout{software giants} regional businesses \pause
        \end{itemize}
      \end{block}
    \end{column}

    \begin{column}{0.31\textwidth}
      \begin{block}{social}
        \begin{itemize}
        \pause
        \item{\sout{backdoors} transparency} \pause
        \item{\sout{tracking} \\ privacy} \pause
        \item{\sout{passive users} community} \pause
        \end{itemize}
      \end{block}
    \end{column}

    \begin{column}{0.31\textwidth}
      \begin{block}{technological}
        \begin{itemize}
        \pause
        \item{\sout{walled gardens} interoperability} \pause
        \item{\sout{stagnation} innovation}
        \end{itemize}
      \end{block}
    \end{column}

  \end{columns}
\end{frame}

\begin{frame}[c]
  \frametitle{Eidgenössisches Finanzdepartement agrees!}
  \pause
  \begin{block}{excerpts from ``Praxis-Leitfaden: Open Source Software in der Bundesverwaltung''\footnote<.(1)->{\url{https://www.bk.admin.ch/dam/bk/de/dokumente/dti/ikt-vorgaben/strategien/oss/Praxis-Leitfaden_OSS_Bundesverwaltung_V_1-0.pdf}} on the potential of Free Software:}
    \pause
    \begin{itemize}
    \item ``Kostenersparnisse durch Kooperationen mit anderen Nutzern'' \pause
    \item ``Niedrigere Herstellerabhängigkeit'' \pause
    \item ``Offene Standards und hohe Interoperabilität'' \pause
    \item ``Sicherheit und Vertrauen durch Transparenz'' \pause
    \item ``Rasche Innovation und Integration'' 
    \item \ldots
    \end{itemize}
    \end{block}
\end{frame}

\subsection{Where do we stand in Switzerland?}

\begin{frame}[c]
  \frametitle{Public institutions using FLOSS}
  \framesubtitle{OneGov GEVER -- Document- \& Recordsmanagement}
  \begin{figure}
    \includegraphics[width=0.95\textwidth]{res/onegovgever.png}
  \end{figure}
  \note[item]{used by cantons of SG, ZG, AI and various municipalities}
  \note[item]{GEVER source code: \url{https://github.com/4teamwork/opengever.core}}
\end{frame}
  
\begin{frame}[c]
  \frametitle{Public institutions using FLOSS}
  \framesubtitle{Caluma -- ``a collaborative form editing and workflow service''}
  \begin{figure}
    \includegraphics[height=0.7\textheight]{res/caluma.png}
  \end{figure}
  \note[item]{software developed by a Swiss company}
  \note[item]{used by cantons of SH and UR for managing building applications}
  \note[item]{``The freedom to develop where it is necessary - for that you absolutely need open software.'' - Paul Walker}
  \note[item]{``the sooner the cantons realise that this is the best way, the better it will be for these cantons.'' - Paul Walker}
  \note[item]{\url{https://fsfe.org/news/2021/news-20210318-01.en.html}}
  \note[item]{caluma source code: \url{https://github.com/projectcaluma/caluma}}
\end{frame}
        
\begin{frame}[c]
  \frametitle{Public institutions using FLOSS}
  \framesubtitle{decidim -- ``a digital platform for citizen participation''}
  \begin{figure}
    \includegraphics[width=0.75\textwidth]{res/dialogluzern.png}
  \end{figure}
  \note[item]{used by the city of Lucerne for \url{dialogluzern.ch}}
  \note[item]{platform where city projects are discussed, events posted, and clubs presented}
  \note[item]{enabling citizens to participate in the city life}
  \note[item]{decicim source code: \url{https://github.com/decidim/decidim}}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions releasing FLOSS}
  \framesubtitle{Legal? Yes!}
  \begin{block}{BSG 152.042 - Verordnung über die Informations- und Telekommunikationstechnik der Kantonsverwaltung (ICTV)\footnote{https://www.belex.sites.be.ch/frontend/versions/1410}}
    \textbf{Artikel 16, Absatz 2}: [Das Amt für Informatik und Organisation] kann im Auftrag der für die Applikation verantwortlichen Behörden Software, über deren Rechte der Kanton verfügt, unter den Bedingungen einer Open-Source-Lizenz veröffentlichen.
  \end{block}
  \note[item]{case of Swiss Federal court releasing case management system as Free Software; company developing a proprietary court case management system opposed this move as it claimed that would distort the marketplace}
  \note[item]{as a result of this discussion, the canton of Bern put it into law that it's applications can be released as Free Software}
  \note[item]{might soon see a similar passage on the federal level in the ``Bundesgesetz über den Einsatz elektronischer Mittel zur Erfüllung von Behördenaufgaben (EMBaG)''}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions releasing FLOSS}
  \framesubtitle{Swiss geo portal -- \url{map.geo.admin.ch}}
  \begin{figure}
    \includegraphics[width=0.8\textwidth]{res/swiss-geo-portal.png}
    \caption{How cool is that?!}
  \end{figure}
  \note[item]{extremely detailed maps full of information; hiking routes, mineral reserves, or tank movement routes}
  \note[item]{webviewer but also queryable API is Free Software: use, study, share, contribute}
  \note[item]{source code: \url{https://github.com/geoadmin}}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions releasing FLOSS}
  \framesubtitle{Covid-19 tracing and certificate apps}
  \begin{figure}
    \includegraphics[height=0.7\textheight]{res/fdroid-chovid.png}
    \caption{F-Droid builds}
  \end{figure}
  \note[item]{from the beginning very clear that these apps would be released as Free Software}
  \note[item]{people still have difficulties trusting these apps with their personal data -- imagine what would have happened with proprietary applications}
  \note[item]{source code of the apps: \url{https://github.com/admin-ch}}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions f*cking up}
  \framesubtitle{Justitia 4.0 \& Platform Justitia.Swiss}
  \begin{block}{excerpt from the tender specifications:}
    \textbf{EKA-L1-12 - Quellcodes} \\ Die Anbieterin erklärt sich bereit, sämtliche Quellcodes sowie deren Dokumentationen der Auftraggeberin abzutreten. [\ldots] Der Anbieterin ist untersagt, die Quellcodes und oder die Spezifikationen ohne Zustimmung der Auftraggeberin weiterzugeben oder anderweitig einzusetzen.
  \end{block}
  \note{Apart from other huge problems with the whole project, developing the platform as Free Software is not permissible with this specification.}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions f*cking up}
  \framesubtitle{Prevalence of proprietary software in education}
  \begin{figure}
    \includegraphics[width=0.75\textwidth]{res/eth-zoom.png}
    \caption{one out of many sad examples}
  \end{figure}
\end{frame}

\begin{frame}[c]
  \frametitle{Public institutions \& FLOSS}
  \framesubtitle{Summary}
  \pause
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{res/oss-empfehlung.png}
    \caption{A start but not enough!}
  \end{figure}
\end{frame}

\subsection{What to do next?}

\begin{frame}[c]
  \frametitle{Sign our open letter\footnote{https://publiccode.eu/openletter/}!}
  \begin{figure}
    \includegraphics[height=0.6\textheight]{res/signatures.png}
    \caption{You'll be in good company!}
  \end{figure}
  \note[item]{more than 30K individuals have signed already}
  \note[item]{accompanied by more than 200 organisations, among them: EFF, Mozilla, CCC, Debian, Tor Project, KDE, Creative Commons, OSI, \ldots}
\end{frame}

\begin{frame}[c]
  \frametitle{Let public institutions know!}
  \begin{figure}
    \includegraphics[height=0.5\textheight]{res/asturias.png}
  \end{figure}
  \pause
  \begin{quote}
    I think for the first time, a lot of politicians understood what Free Software is. -- Iyán Méndez Veiga\footnote<.(1)->{\url{https://fsfe.org/news/2019/news-20190514-01.en.html}}
  \end{quote}
  \note[item]{hacktivists from the province of Asturias, Spain convinced their local parliament to sign the PMPC open letter; not legally binding, it is a strong symbolic act}
  \note[item]{politicians are not necessarily malicious, in most of the cases they might just be ignorant about the issue[s] of software freedom}
\end{frame}

\begin{frame}[c]
  \frametitle{Lead by example}
  \framesubtitle{Stop using proprietary software yourself!}
  \begin{figure}
    \includegraphics[width=0.8\textwidth]{res/floss-icons.png}
  \end{figure}
  \note{from top left to bottom right: LaTeX, Firefox, Libre Office, Debian, Lineage OS, Python, VLC player, Blender, Mastodon, git, Emacs, Audacity, GIMP, Inkscape}
\end{frame}


\begin{frame}[c]
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{res/infrastructures_large.png}
    \caption{Infrastructures\\\footnotesize{\url{https://xkcd.com/743/}}}
  \end{figure}
\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
