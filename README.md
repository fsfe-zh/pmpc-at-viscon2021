# Public Money Public Code @ VIScon2021

This repository contains the source for the "Public Money Public Code" presentation held at VIScon 2021 by the FSFE Zurich local group.

## Create presentation .pdf

To create the presentation .pdf, run

```bash
$ latexmk -pdf pmpc-at-viscon2021.tex
```

Make sure you have `latexmk`, `pdflatex`, and all the necessary `texlive` packages installed.
